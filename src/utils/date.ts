export const isToday = (date: string) => {
  const today = new Date();
  const [year, month, day] = date.split("-");

  return (
    parseInt(year) === today.getFullYear() &&
    parseInt(month) === today.getMonth() + 1 &&
    parseInt(day) === today.getDate()
  );
};
