import {
  Button,
  Card,
  FormElement,
  Input,
  Radio,
  Link as UILink,
} from "@nextui-org/react";
import { Link } from "react-router-dom";
import useLocalStorage from "../../hooks/useLocalStorage";
import useIsMobileScreen from "../../hooks/useMobileScreen";
import { TypeState } from "../../models/task";
import FilterLayout from "../layout/FilterLayout";

const Filter = () => {
  const {
    isDueToday,
    restoreFilteredData,
    filterByState,
    filterByTag,
    filterById,
  } = useLocalStorage();

  const isMobile = useIsMobileScreen();

  const handleStateFilter = (val: string) => filterByState(val as TypeState);
  const handleIdFilter = (e: React.ChangeEvent<FormElement>) => {
    if (e.target.value) {
      filterById(parseInt(e.target.value));
      return;
    }
    restoreFilteredData();
  };
  const handleTagFilter = (e: React.ChangeEvent<FormElement>) =>
    filterByTag(e.target.value);

  return (
    <Card>
      <FilterLayout>
        <Input
          fullWidth={isMobile}
          label="Filtrar por id"
          placeholder="id"
          size="xs"
          onChange={handleIdFilter}
        />
        <Input
          fullWidth={isMobile}
          label="Filtrar por nombre de etiqueta"
          placeholder="tag name"
          size="xs"
          onChange={handleTagFilter}
        />
        <Radio.Group
          size="xs"
          orientation="horizontal"
          onChange={handleStateFilter}
          label="Estado"
        >
          <Radio value="undefined">Indefinido</Radio>
          <Radio value="over">Terminado</Radio>
        </Radio.Group>

        <Button size="xs" onClick={isDueToday} color="error" >
          Vence hoy
        </Button>
        <Button size="xs" onClick={restoreFilteredData}>
          Restaurar datos
        </Button>
        <Link to="task">
          <UILink block color="warning">
            Agregar
          </UILink>
        </Link>
      </FilterLayout>
    </Card>
  );
};

export default Filter;
