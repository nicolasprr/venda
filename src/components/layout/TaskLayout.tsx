import styled from "styled-components";
import { devices } from "../../theme";

const TaskLayout = styled.div`
    display: flex;
    flex-direction: column;
    gap: 16px;
    justify-content: center;
    margin: 0 30%;
    @media ${devices.tablet} {
        flex-direction: column;
        padding: 4px;
        align-items: flex-start;
        margin: 0
  }

`

export default TaskLayout