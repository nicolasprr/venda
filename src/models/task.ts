import { ITag } from "./tag";

export type TypeState = "over" | "undefined" | "";

export interface ITask {
  id: number;
  description: string;
  endDate: string;
  state: TypeState;
  tag: ITag[];
}
