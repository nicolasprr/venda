import styled from "styled-components";

import { devices } from "../../theme";

const FilterLayout = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  gap: 16px;
  padding: 16px;
  @media ${devices.tablet} {
    flex-direction: column;
    padding: 4px;
    align-items: flex-start;
  }
`;

export default FilterLayout;
