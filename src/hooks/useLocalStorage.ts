import { useEffect } from "react";
import { ITask, TypeState } from "../models/task";
import { useTasks } from "../providers/TaskProvider";
import { isToday } from "../utils/date";

const storageKey = "tasks";

const useLocalStorage = () => {
  const { tasks, setTasks, filteredTasks, setFilteredTasks } = useTasks();

  useEffect(() => {
    const storagedTasks = getTasksFromLocalStorage();
    setTasks?.(storagedTasks);
    setFilteredTasks?.(storagedTasks);
  }, []);

  const getTasksFromLocalStorage = () => {
    const storagedTask = localStorage.getItem(storageKey);
    const isIninitalized = !!storagedTask;

    if (isIninitalized) {
      const storagedTask = JSON.parse(
        localStorage.getItem(storageKey) || "[]"
      ) as ITask[];

      return storagedTask;
    }
    return [];
  };

  const addTask = (task: ITask) => {
    const alreadyExists = !!tasks.find(
      (storedTask) => storedTask.id === task.id
    );
    if (!alreadyExists) {
      const newTaskList = [...tasks, task].sort(
        (prev, curr) => prev.id - curr.id
      );
      setTasks?.(newTaskList);

      localStorage.setItem(storageKey, JSON.stringify(newTaskList));
    }
    //todo: add error
  };

  const updateTask = (task: ITask) => {
    const copiedTask = [...tasks];
    const indexedTask = tasks.findIndex((curr) => curr.id === task.id);
    copiedTask[indexedTask] = task;
    setTasks?.(copiedTask);
    localStorage.setItem(storageKey, JSON.stringify(copiedTask));
  };

  const deleteTask = (id: number) => {
    const copiedTask = [...tasks];
    const copiedFilteredTask = [...filteredTasks];

    const indexedTask = tasks.findIndex((curr) => curr.id === id);
    const indexedFilterdTask = filteredTasks.findIndex(
      (curr) => curr.id === id
    );

    copiedTask.splice(indexedTask, 1);
    copiedFilteredTask.splice(indexedFilterdTask, 1);

    setTasks?.(copiedTask);
    setFilteredTasks?.(copiedFilteredTask);
    localStorage.setItem(storageKey, JSON.stringify(copiedTask));
  };

  const getTask = (id: number) => {
    const task = getTasksFromLocalStorage().find((task) => task.id === id);
    return task;
  };

  //!filters

  const filterById = (id: number) => {
    const filteredTasks = tasks.filter((task) =>
      String(task.id).includes(String(id))
    );
    setFilteredTasks?.(filteredTasks);
  };

  const filterByTag = (tagName: string) => {
    const filteredTasks = tasks.filter((task) =>
      task.tag.some((tag) => tag.name.includes(tagName))
    );
    setFilteredTasks?.(filteredTasks);
  };

  const filterByState = (state: TypeState) => {
    const filteredTasks = tasks.filter((task) => task.state === state);
    setFilteredTasks?.(filteredTasks);
  };

  const isDueToday = () => {
    const filteredTasks = tasks.filter((task) => isToday(task.endDate));

    setFilteredTasks?.(filteredTasks);
  };
  const restoreFilteredData = () => {
    setFilteredTasks?.(tasks);
  };

  return {
    tasks,
    deleteTask,
    updateTask,
    addTask,
    filterByTag,
    filterByState,
    isDueToday,
    restoreFilteredData,
    filterById,
    filteredTasks,
    getTask,
  };
};

export default useLocalStorage;
