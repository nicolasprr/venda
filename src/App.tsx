import { BrowserRouter, Route } from "react-router-dom";
import { Routes } from "react-router";
import { ThemeProvider } from "styled-components";

import "typeface-lato";
import TaskProvider from "./providers/TaskProvider";
import Home from "./components/pages/Home";
import TaskForm from "./components/pages/TaskForm";
import { GlobalStyle } from "./theme";

function App() {
  return (
    <ThemeProvider theme={{}}>
      <GlobalStyle />
      <TaskProvider>
        <BrowserRouter>
          <Routes>
            <Route index element={<Home />} />
            <Route path="task" element={<TaskForm />} />
            <Route path="task/:id" element={<TaskForm />} />
          </Routes>
        </BrowserRouter>
      </TaskProvider>
    </ThemeProvider>
  );
}

export default App;
