# Prueba Bold

## 1. Librerias

#### styled-components

Ofrece un enfoque más declarativo por lo que ayuda a mejorar la experiencia de desarrollo

#### typeface

Te permite tener las fuentes en node_modules

## 2. ¿Cómo correr el proyecto?

```console
yarn or npm i
```

#### dev mode

```console
yarn dev or npm dev
```

#### build and preivew

```console
yarn build or npm build
```

```console
yarn preview or npm preview
```

Una vez ejecutado estos comandos deberias ser capaz de ver
http://127.0.0.1:4173/




### Alias

Podemos usar absolute imports para remplazar las rutas relativas ../..


### ¿Cómo decidió las opciones técnicas y arquitectónicas utilizadas como parte de su solución?


#### Vite
Vite permite tener un desarrollo más comodo debido a su performance

#### styled components

Permite css in js, debido a que su uso permite hacer los componentes declarativos de manera mas eficiente y ademas tiene un excelente soporte para temas y tipado seguro

#### Next ui

Es una libreria nueva que merece ser 

#### typefaces

Permite una configuracion  y puesta a produccion rapida




### ¿Hay alguna mejora que pueda hacer en su envío?

Por supuesto, se puede eslint y prettier para tener un entorno de desarrollo mas organizado con el equipo, esto permite como tal tener ciertas reglas especificadas

Se puede hacer uso de husky para optimizar ciertas tareas

Mejorar el tema, actualmente no se cuenta con un tema en especifico, se pueden crear reglas para especificar dimensiones, colores y acceder a ellos de manera facil

Validación de formulario, aunque actualmente es funcional existen mejores soluciones como react hook form que ahorra el tiempo de validar cada uno de los campos, ademas se puede agregar el uso de yup para que la escritura de las reglas de validacion sean mucho mas eficientes y legibles



### ¿Como mejorar la aplicación?

Se pueden agregar animaciones  con librerias como framer motion y si es el caso usando css

Se puede mejorar el uso de las etiquetas, haciendo un CRUD de esta feature de tal manera que la vinculacion de las etiquetas con la tarea no sea tan tediosa

