import HomeLayout from "../layout/HomeLayout";
import Filter from "../templates/Filter";
import Tasks from "../templates/Tasks";

const Home = () => {
  return (
    <HomeLayout>
      <Filter />
      <Tasks />
    </HomeLayout>
  );
};

export default Home;
