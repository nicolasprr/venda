import styled from "styled-components";
import useLocalStorage from "../../hooks/useLocalStorage";
import Task from "../organisms/Task";

const TasksLayout = styled.div`
  display: flex;
  gap: 8px;
  flex-wrap: wrap;
  align-items: center;
  
`;

const Tasks = () => {
  const { filteredTasks } = useLocalStorage();

  return (
    <TasksLayout>
      {filteredTasks.map((item) => (
        <Task key={item.id} task={item} />
      ))}
    </TasksLayout>
  );
};

export default Tasks;
