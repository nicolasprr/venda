import {
  createContext,
  ReactNode,
  useContext,
  useState,
  Dispatch,
  SetStateAction,
} from "react";
import { ITask } from "../models/task";

interface ICreateContext {
  tasks: ITask[];
  filteredTasks: ITask[];
  setTasks?: Dispatch<SetStateAction<ITask[]>>;
  setFilteredTasks?: Dispatch<SetStateAction<ITask[]>>;
}

const ContextTask = createContext<ICreateContext>({ tasks: [], filteredTasks: [] });

interface Props {
  children: ReactNode;
}

const TaskProvider = ({ children }: Props) => {
  const [tasks, setTasks] = useState<ITask[]>([]);
  const [filteredTasks, setFilteredTasks] = useState<ITask[]>([]);

  return (
    <ContextTask.Provider value={{ tasks, setTasks, filteredTasks, setFilteredTasks }}>
      {children}
    </ContextTask.Provider>
  );
};

export const useTasks = () => {
  const tasks = useContext(ContextTask);
  return tasks;
};
export default TaskProvider;
