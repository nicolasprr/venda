import { memo } from "react";
7;
import {
  Badge,
  BadgeProps,
  Button,
  Card,
  Link as UILink,
  Progress,
  Text,
} from "@nextui-org/react";
import styled from "styled-components";
import { ITask } from "../../models/task";
import { Link } from "react-router-dom";
import useLocalStorage from "../../hooks/useLocalStorage";
import { devices } from "../../theme";

interface Props {
  task: ITask;
}

const CardContainer = styled(Card)`
  padding: 16px;
  width: 250px;
  word-wrap: break-word;
  background-color: white;
  border-radius: 16px;
  @media ${devices.tablet} {
    width: 100%;
    flex-direction: column;
    max-width: none;
  }
`;
const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const TagContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 4px;
`;

const CTAContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Task = ({ task }: Props) => {
  const { deleteTask } = useLocalStorage();
  const { description, endDate, id, state, tag } = task;

  const overText = state === "over" ? "Terminado" : "No definido";

  const colors = [
    "default",
    "primary",
    "secondary",
    "error",
    "success",
    "warning",
  ] as BadgeProps["color"][];

  const progressColor = state === "over" ? "success" : "primary";

  const handleDeleteTask = () => {
    deleteTask(id);
  };

  return (
    <CardContainer>
      <HeaderContainer>
        <Text b color="primary">
          {id}
        </Text>
        <TagContainer>
          {tag.map(
            (item) =>
              item.name && (
                <Badge
                  size="xs"
                  color={colors[Math.floor(colors.length * Math.random())]}
                >
                  {item.name}
                </Badge>
              )
          )}
        </TagContainer>
      </HeaderContainer>
      <Progress value={100} size="xs" color={progressColor} />
      <Text> {description}</Text>
      <Text b> {endDate}</Text>
      <Badge size="sm">{overText}</Badge>
      <CTAContainer>
        <Link to={`task/${id}`}>
          <UILink block color="warning">
            Editar
          </UILink>
        </Link>
        <Button size="xs" color="error" onClick={handleDeleteTask}>
          Borrar
        </Button>
      </CTAContainer>
    </CardContainer>
  );
};

export default memo(Task);
