import {
  Input,
  Textarea,
  Switch,
  Button,
  Text,
  Badge,
} from "@nextui-org/react";
import styled from "styled-components";
import useIsMobileScreen from "../../hooks/useMobileScreen";

import useTaskForm from "../../hooks/useTaskForm";
import TaskLayout from "../layout/TaskLayout";

const IsOverContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  gap: 8px;
  align-items: center;
`;

const ButtonsContainer = styled.div`
  display: flex;
  width: fit-content;
  gap: 16px;
`;

const TaskForm = () => {
  const {
    tags,
    task,
    tagInfo,
    addTag,
    onSubmitForm,
    handleOverSwitch,
    handleTagInfo,
    handleTaskInfo,
    isOver,
    ButtonText,
    errors,
  } = useTaskForm();

  const isMobile = useIsMobileScreen();

  return (
    <TaskLayout>
      <Text b> {ButtonText} </Text>
      <Input
        fullWidth={isMobile}
        size="xs"
        clearable
        bordered
        placeholder="id"
        label="ID"
        name="id"
        type="number"
        value={task.id}
        onChange={handleTaskInfo}
      />
      <Textarea
        fullWidth={isMobile}
        bordered
        size="xs"
        value={task.description}
        onChange={handleTaskInfo}
        minRows={3}
        placeholder="Descripción"
        label="Description"
        name="description"
      />
      <Input
        size="xs"
        fullWidth={isMobile}
        value={task.endDate}
        onChange={handleTaskInfo}
        type="date"
        placeholder="2022/20/02"
        label="end date"
        name="endDate"
      />
      <IsOverContainer>
        <Text b size="$base">
          Terminado
        </Text>
        <Switch size="xs" checked={isOver} onChange={handleOverSwitch} />
      </IsOverContainer>
      <Input
        size="xs"
        name="tagName"
        onChange={handleTagInfo}
        bordered
        label="Tag name"
        value={tagInfo.tagName}
        placeholder="Tag nameeeee"
      />
      <Input
        size="xs"
        onChange={handleTagInfo}
        name="id"
        value={tagInfo.id}
        color="secondary"
        label="Tag id"
        clearable
        bordered
        type="number"
        placeholder="tag id"
        width="100px"
      />
      <ButtonsContainer>
        <Button size="xs" color="primary" onClick={addTag}>
          Agregar una etiqueta
        </Button>
        <Button bordered size="xs" color="error" onClick={onSubmitForm}>
          {ButtonText}
        </Button>
      </ButtonsContainer>
      {tags.map((tag) => (
        <Badge> {tag.name} </Badge>
      ))}

      {errors?.map((error) => (
        <Text size="x-small" key={error} b color="error">
          {error}
        </Text>
      ))}
    </TaskLayout>
  );
};

export default TaskForm;
