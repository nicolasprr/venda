export const isEmptyError = (value: string | number, fieldName: string) => {
  if (!value) {
    return {
      message: `El campo no debe ir vacio (${fieldName})`,
    };
  }
  return false;
};
