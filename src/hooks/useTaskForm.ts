import { FormElement } from "@nextui-org/react";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { ITag } from "../models/tag";
import { ITask } from "../models/task";
import { isEmptyError } from "../utils/errors";
import useLocalStorage from "./useLocalStorage";

const useTaskForm = () => {
  const { addTask, getTask, updateTask } = useLocalStorage();
  const { id } = useParams();
  const navigate = useNavigate();

  const [isOver, setIsOver] = useState<boolean>(false);
  const [tags, setTags] = useState<ITag[]>([]);

  const [tagInfo, setTagInfo] = useState({
    tagName: "",
    id: "",
  });

  const [errors, setErrors] = useState<string[]>([]);

  const [task, setTaskInfo] = useState<ITask>({
    id: 0,
    description: "",
    endDate: "",
    state: "",
    tag: [],
  });

  const isValidTask = () => {
    const idError = isEmptyError(task.id, "ID");
    const descriptionError = isEmptyError(task.description, "descripción");
    const dateError = isEmptyError(task.endDate, "fecha");
    const errors = [
      ...(idError ? [idError.message] : []),
      ...(descriptionError ? [descriptionError.message] : []),
      ...(dateError ? [dateError.message] : []),
    ];

    setErrors(errors);

    return errors.length === 0;
  };

  useEffect(() => {
    if (id) {
      const task = getTask(parseInt(id));
      if (!task) return;
      setTags(task.tag);
      setIsOver(task.state === "over");
      setTaskInfo(task);
    }
  }, []);

  const handleOverSwitch = () => {
    setIsOver((prev) => !prev);
  };

  const handleTaskInfo = (e: React.ChangeEvent<FormElement>) => {
    const { name, value } = e.target;
    if (name === "id") {
      setTaskInfo((prev) => ({
        ...prev,
        id: parseInt(value),
      }));
      return;
    }
    setTaskInfo((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleAddTask = () => {
    addTask({ ...task, tag: tags, state: isOver ? "over" : "undefined" });
  };

  const handleUpdateTask = () => {
    updateTask({ ...task, tag: tags, state: isOver ? "over" : "undefined" });
  };

  const addTag = () => {
    const newTag: ITag = {
      id: parseInt(tagInfo.id),
      name: tagInfo.tagName,
    };
    setTags((prev) => [...prev, newTag]);
    setTagInfo({
      tagName: "",
      id: "",
    });
  };

  const handleTagInfo = (e: React.ChangeEvent<FormElement>) => {
    const { name, value } = e.target;
    setTagInfo((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const onSubmitForm = () => {
    if (!isValidTask()) return;

    if (id) {
      handleUpdateTask();
      navigate("/");
      return;
    }
    handleAddTask();
    navigate("/");
  };

  const ButtonText = id ? "Actualizar tarea" : "Crear tarea";

  return {
    task,
    tags,
    handleOverSwitch,
    handleAddTask,
    handleTaskInfo,
    addTag,
    tagInfo,
    isOver,
    handleTagInfo,
    handleUpdateTask,
    ButtonText,
    onSubmitForm,
    errors,
  };
};

export default useTaskForm;
