import { useEffect, useState } from "react";
import { sizes} from "../theme";

const useIsMobileScreen = () => {
  const [isMobileS, setIsMobileS] = useState<boolean>(false);

  useEffect(() => {
    const handleMobilePort = () => {
      const windowWidth = window.innerWidth;
      if (windowWidth < Number(sizes.tablet.replace("px", ""))) {
        setIsMobileS(true);
        return;
      }
      setIsMobileS(false);
    };

    handleMobilePort();
    window.addEventListener("resize", handleMobilePort);
    return () => window.removeEventListener("resize", handleMobilePort);
  }, []);
  return isMobileS;
};

export default useIsMobileScreen;
