import styled from "styled-components";

const HomeLayout = styled.div`
    display: flex;
    height: 100%;
    flex-direction: column;
    gap: 10px;
`

export default HomeLayout;